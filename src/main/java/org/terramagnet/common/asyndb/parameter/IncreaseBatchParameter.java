package org.terramagnet.common.asyndb.parameter;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class IncreaseBatchParameter implements BatchParameter {

    private final Map<String, Object[]> counter = new HashMap<String, Object[]>();
    private int length = -1;
    private String sql;

    public IncreaseBatchParameter() {
    }

    public IncreaseBatchParameter(String sql) {
	this.sql = sql;
    }

    @Override
    public List<Object[]> toArgs() {
	synchronized (counter) {
	    Collection<Object[]> values = counter.values();
	    LinkedList<Object[]> list = new LinkedList<Object[]>(values);
	    counter.clear();
	    return list;
	}
    }

    @Override
    public String getSql() {
	return sql;
    }

    public void setSql(String sql) {
	this.sql = sql;
    }

    @Override
    public synchronized boolean add(Object[] e) {
	String key = hash(e);
	Object[] value = counter.get(key);
	if (value == null) {
	    value = new Object[e.length + 1];
	    counter.put(key, value);
	    value[0] = 1;
	    for (int i = 0; i < e.length; i++) {
		Object o = e[i];
		value[i + 1] = e[i];
	    }
	} else {
	    int c = (Integer) value[0];
	    value[0] = c + 1;
	}
	if (length == -1) {
	    length = e.length;
	}
	return true;
    }

    @Override
    public void clear() {
	counter.clear();
    }

    @Override
    public boolean isEmpty() {
	return counter.isEmpty();
    }

    @Override
    public int length() {
	return length;
    }

    @Override
    public int size() {
	return counter.size();
    }

    private String hash(Object[] args) {
	if (args == null || args.length == 0) {
	    return null;
	}
	StringBuilder builder = new StringBuilder();
	for (Object obj : args) {
	    builder.append(obj.getClass().hashCode()).append(":").append(obj.hashCode()).append("-");
	}
	return builder.substring(0, builder.length() - 1);
    }
}

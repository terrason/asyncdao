package org.terramagnet.common.asyndb.parameter;

import java.util.List;

public interface BatchParameter {

    public String getSql();

    public List<Object[]> toArgs();

    public int length();

    public int size();

    public boolean add(Object[] e);

    public void clear();

    public boolean isEmpty();
}

package org.terramagnet.common.asyndb.parameter;

import java.util.LinkedList;
import java.util.List;

public class DefaultBatchParameter implements BatchParameter {

    private List<Object[]> args = new LinkedList<Object[]>();
    private int length = -1;
    private String sql;

    public DefaultBatchParameter() {
    }

    public DefaultBatchParameter(String sql) {
	this.sql = sql;
    }

    @Override
    public synchronized List<Object[]> toArgs() {
	List<Object[]> rtnVal = args;
	args = new LinkedList<Object[]>();
	return rtnVal;
    }

    @Override
    public String getSql() {
	return sql;
    }

    public void setSql(String sql) {
	this.sql = sql;
    }

    @Override
    public boolean isEmpty() {
	return args.isEmpty();
    }

    @Override
    public synchronized boolean add(Object[] e) {
	if (length == -1) {
	    length = e.length;
	}
	return args.add(e);
    }

    @Override
    public synchronized void clear() {
	args.clear();
    }

    @Override
    public int length() {
	return length;
    }

    @Override
    public int size() {
	return args.size();
    }

}

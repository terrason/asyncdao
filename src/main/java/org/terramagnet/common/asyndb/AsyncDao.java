/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.terramagnet.common.asyndb;


public interface AsyncDao {

    /**
     * 设置异步线程执行周期.
     *
     * @param period 执行周期（单位：毫秒）
     */
    public void setPeriod(int period);

    /**
     * 立即执行所有sql语句. （同步执行）
     */
    public void synchronizeAll();

    /**
     * 执行SQL语句. 语句将会异步执行。
     *
     * @param sql 语句
     * @param parameters SQL参数
     */
    public void update(String sql, Object... parameters);
    
}

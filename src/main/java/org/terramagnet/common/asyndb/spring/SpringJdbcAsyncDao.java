package org.terramagnet.common.asyndb.spring;

import org.terramagnet.common.asyndb.AsyncDao;
import org.terramagnet.common.asyndb.parameter.DefaultBatchParameter;
import org.terramagnet.common.asyndb.parameter.IncreaseBatchParameter;
import org.terramagnet.common.asyndb.parameter.BatchParameter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * 异步执行SQL语句.
 *
 * @author lip
 */
@Repository("springJdbcAsyncDao")
public class SpringJdbcAsyncDao implements AsyncDao {

    /**
     * 执行SQL语句. 语句将会异步执行。
     *
     * @param sql 语句
     * @param parameters SQL参数
     */
    @Override
    public void update(String sql, Object... parameters) {
	if(jdbcTemplate==null){
	    throw new IllegalStateException("jdbcTemplate unset yet!");
	}
	BatchParameter cachedSqlParams;
	synchronized (sqlCache) {
	    cachedSqlParams = sqlCache.get(sql);
	    if (cachedSqlParams == null) {
		cachedSqlParams = createCache(sql);
		sqlCache.put(sql, cachedSqlParams);
	    }
	}
	if (!cachedSqlParams.isEmpty() && cachedSqlParams.length() != parameters.length) {
	    throw new IllegalArgumentException("invalid parameter size: " + sql + " must accepts " + cachedSqlParams.length() + " parameters,not " + parameters.length + "!");
	}
	cachedSqlParams.add(parameters);
	if (asyncExecutor == null) {
	    setupAsyncExecutor();
	}
    }

    /**
     * 立即执行所有sql语句.
     */
    @Override
    public void synchronizeAll() {
	synchronized (sqlCache) {
	    for (BatchParameter batchParameter : sqlCache.values()) {
		if (!batchParameter.isEmpty()) {
		    jdbcTemplate.batchUpdate(batchParameter.getSql(), batchParameter.toArgs());
		}
	    }
	}
    }

    private BatchParameter createCache(String sql) {
	Matcher matcher = pattern.matcher(sql);
	if (matcher.find()) {
	    String s = matcher.replaceFirst("$1?$4");
	    return new IncreaseBatchParameter(s);
	}
	return new DefaultBatchParameter(sql);
    }

    private void setupAsyncExecutor() {
	asyncExecutor = new Thread(new Runnable() {

	    @Override
	    public void run() {
		while (true) {
		    try {
			synchronizeAll();
			Thread.sleep(period);
		    } catch (Exception ex) {
			logger.warn("异步SQL执行线程意外终止", ex);
			break;
		    }
		}
	    }
	}, "异步SQL执行线程");
	asyncExecutor.setDaemon(true);
	asyncExecutor.start();
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 设置异步线程执行周期.
     *
     * @param period 执行周期（单位：毫秒）
     */
    @Override
    public void setPeriod(int period) {
	this.period = period;
    }
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private int period = 60000;
    private Thread asyncExecutor;

    private final Map<String, BatchParameter> sqlCache = new LinkedHashMap<String, BatchParameter>();
    private final Pattern pattern = Pattern.compile("(update\\s+[\\p{Alnum}_`]+\\s+set\\s+([\\p{Alnum}_`]+)\\s*=\\s*\\2\\s*\\[+-]\\s*)(1)(\\s+where.+)");
    private final Logger logger = LoggerFactory.getLogger(SpringJdbcAsyncDao.class);
}
